package com.woniuxy;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.woniuxy.dao")
public class Day11SpringbootMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(Day11SpringbootMybatisApplication.class, args);
    }

}
