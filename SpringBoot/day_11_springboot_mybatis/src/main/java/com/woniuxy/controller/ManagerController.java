package com.woniuxy.controller;

import com.woniuxy.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName: ManagerController
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/14 17:49
 */
@Controller
@RequestMapping("/managerT")
public class ManagerController {
    @Autowired
    ManagerService service;
    @RequestMapping("/findAll")
    @ResponseBody
    public String managers(){
        service.findAll();
        return "s";
    }
}
