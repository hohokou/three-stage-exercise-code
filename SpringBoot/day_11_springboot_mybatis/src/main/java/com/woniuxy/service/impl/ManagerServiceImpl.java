package com.woniuxy.service.impl;

import com.woniuxy.dao.ManagerDao;
import com.woniuxy.entity.RbacManager;
import com.woniuxy.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName: ManagerServiceImpl
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/14 19:07
 */
@Service
public class ManagerServiceImpl implements ManagerService {

    @Autowired
    private ManagerDao managerDao;
    @Override
    public void findAll() {
        System.out.println(managerDao.findAll());
    }
}
