package com.woniuxy.dao;

import com.woniuxy.entity.RbacManager;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @ClassName: ManagerDao
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/14 19:19
 */
@Mapper
public interface ManagerDao {
    List<RbacManager> findAll();
}
