package com.woniuxy.utils;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;

/**
 * @ClassName: TimeFilter
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/15 11:57
 */
@Component
@WebFilter(urlPatterns = "/*",filterName = "timeFilter")
public class TimeFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        long start = System.currentTimeMillis();
        filterChain.doFilter(servletRequest, servletResponse);
        System.out.println("请求耗时"+(System.currentTimeMillis() - start)+"MS");
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}
