package com.woniuxy.config;

import com.woniuxy.utils.TimeFilter2;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

/**
 * @ClassName: FilterRegisterBean
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/15 12:50
 */
@Configuration
public class FilterRegisterBean {

    @Bean
    public FilterRegistrationBean<TimeFilter2> timeFilter2(){
        FilterRegistrationBean<TimeFilter2> filter = new FilterRegistrationBean<>();
        //将过滤器放入系统的过滤器注册器中
        filter.setFilter(new TimeFilter2());
        //设置拦截请求
        filter.addUrlPatterns("/*");
        //设置过滤器优先级
        filter.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return filter;
    }
}
