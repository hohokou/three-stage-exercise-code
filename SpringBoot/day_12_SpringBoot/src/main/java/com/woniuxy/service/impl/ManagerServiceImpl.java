package com.woniuxy.service.impl;

import com.woniuxy.dao.ManagerDao;
import com.woniuxy.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassName: ManagerServiceImpl
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/14 19:07
 */
@Service
public class ManagerServiceImpl implements ManagerService {

    @Autowired
    private ManagerDao managerDao;
    @Override
    public String findAll() {
        return managerDao.findAll().toString();
    }
}
