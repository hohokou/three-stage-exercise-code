package com.woniuxy.entity;


import lombok.Data;

@Data
public class RbacManager {
    private Integer id;
    private String account;
    private String password;
    private String status;
    private Integer roleId;
    private String roles;
}
