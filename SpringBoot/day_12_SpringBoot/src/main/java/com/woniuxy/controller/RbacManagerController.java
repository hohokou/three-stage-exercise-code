package com.woniuxy.controller;

import com.woniuxy.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: RbacManagerController
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/15 11:54
 */
@RestController
@RequestMapping("/rbacManager")
public class RbacManagerController {
    @Autowired
    ManagerService managerService;
    @RequestMapping("/findAll")
    public String findAll(){
        return managerService.findAll();
    }
}
