package com.woniuxy.controller;

import com.woniuxy.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @ClassName: TestController
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/14 11:18
 */
@Controller
@RequestMapping("/test")
public class TestController {

    @RequestMapping("/method01")
    @ResponseBody
    public String method01(){
        return "成功";
    }

    @RequestMapping("/from")
    @ResponseBody
    public String formDate(String date){
        System.out.println(date);
        return "null";
    }

    @RequestMapping("/json")
    @ResponseBody
    public String jsonDate(@RequestBody User user){
        System.out.println(user);
        return "null";
    }
}
