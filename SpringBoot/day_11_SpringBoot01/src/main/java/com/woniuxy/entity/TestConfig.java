package com.woniuxy.entity;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName: TestConfig
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/14 15:01
 */

@Data
////注册成bean 也可以通过在入口加@ConfigurationPropertiesScan("com.woniuxy.entity")也能自动帮你完成注册。
@Configuration
//@ConfigurationProperties(prefix = "TestConfig")
public class TestConfig {
    @Value("${TestConfig.url}")
    private String url;
    @Value("${TestConfig.port}")
    private String port;
}
