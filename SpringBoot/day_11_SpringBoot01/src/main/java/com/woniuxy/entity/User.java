package com.woniuxy.entity;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName: User
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/14 12:01
 */
@Data
public class User {
    private Date birthday;
}
