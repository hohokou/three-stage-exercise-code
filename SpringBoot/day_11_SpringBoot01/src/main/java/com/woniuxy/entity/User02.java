package com.woniuxy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName: User02
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/14 16:22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User02 {
    private String name;
}
