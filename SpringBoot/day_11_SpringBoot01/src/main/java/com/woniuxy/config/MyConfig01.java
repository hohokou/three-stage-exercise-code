package com.woniuxy.config;

import com.woniuxy.entity.User02;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName: MyConfig01
 * @Description:@bean 测试
 * @author: HOHOKOU
 * @date: 2022/2/14 16:19
 */
@Configuration
public class MyConfig01 {

    @Bean
    public User02 user02(){
        return new User02();
    }
}
