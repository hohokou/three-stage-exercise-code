package com.woniuxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
//@ConfigurationPropertiesScan("com.woniuxy.entity")
public class Day11SpringBoot01Application {

    public static void main(String[] args) {
        SpringApplication.run(Day11SpringBoot01Application.class, args);
    }

}
