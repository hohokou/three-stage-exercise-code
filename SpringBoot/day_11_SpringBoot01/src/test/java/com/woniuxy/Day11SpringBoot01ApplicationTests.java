package com.woniuxy;

import com.woniuxy.entity.TestConfig;
import com.woniuxy.entity.User02;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Day11SpringBoot01ApplicationTests {

    @Autowired
    User02 user02;
    @Test
    void contextLoads() {
        System.out.println(user02);
    }

}
