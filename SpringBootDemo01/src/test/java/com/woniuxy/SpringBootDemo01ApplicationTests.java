package com.woniuxy;

import com.woniuxy.service.RbacManagerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringBootDemo01ApplicationTests {

    @Autowired
    RbacManagerService rbacManagerService;
    @Test
    void contextLoads() {
        System.out.println(rbacManagerService.findAll(1, 2, null));
    }

}
