package com.woniuxy.entity;

import lombok.Data;

/**
 * @ClassName: ResponseEntity
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/21 15:19
 */
@Data
public class ResponseEntity <T>{
    int code;
    String message;
    T data;
}
