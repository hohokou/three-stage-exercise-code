package com.woniuxy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * (RbacManager)实体类
 *
 * @author makejava
 * @since 2022-02-21 15:13:38
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RbacManager implements Serializable {
    private static final long serialVersionUID = -26591277256160382L;
    
    private Integer id;
    
    private String account;
    
    private String password;
    
    private String status;
    
    private Integer roleId;

    private String roles;


}

