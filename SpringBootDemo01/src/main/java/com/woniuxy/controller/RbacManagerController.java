package com.woniuxy.controller;

import com.woniuxy.entity.RbacManager;
import com.woniuxy.entity.ResponseEntity;
import com.woniuxy.service.RbacManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName: RbacManagerController
 * @Description: RbacManagerController
 * @author: HOHOKOU
 * @date: 2022/2/21 15:18
 */
@RestController
@RequestMapping("/rbacManager")
public class RbacManagerController {
    @Autowired
    RbacManagerService rbacManagerService;

    @PostMapping("/findAll/{currentPage}/{pageSize}")
    public ResponseEntity findAll(@PathVariable("currentPage") Integer currentPage, @PathVariable("pageSize") Integer pageSize, @RequestBody RbacManager rbacManager) {
        System.out.println(rbacManager);
        return rbacManagerService.findAll(currentPage, pageSize, rbacManager);
    }

    @DeleteMapping("/delUser/{id}")
    public ResponseEntity delUser(@PathVariable int id) {
        return rbacManagerService.delUser(id);
    }

    @PostMapping("/addUser")
    public ResponseEntity addUser(@RequestBody RbacManager rbacManager) {
        return rbacManagerService.addUser(rbacManager);
    }

    @PostMapping("/updateUser")
    public ResponseEntity updateUser(@RequestBody RbacManager rbacManager) {
        return rbacManagerService.updateUser(rbacManager);
    }
}
