package com.woniuxy.service;

import com.woniuxy.entity.RbacManager;
import com.woniuxy.entity.ResponseEntity;

public interface RbacManagerService {

    ResponseEntity findAll(Integer currentPage, Integer pageSize, RbacManager rbacManager);

    ResponseEntity delUser(int id);

    ResponseEntity addUser(RbacManager rbacManager);

    ResponseEntity updateUser(RbacManager rbacManager);
}
