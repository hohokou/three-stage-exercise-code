package com.woniuxy.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniuxy.dao.RbacManagerDao;
import com.woniuxy.entity.RbacManager;
import com.woniuxy.entity.ResponseEntity;
import com.woniuxy.service.RbacManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName: RbacManagerServiceImpl
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/21 15:25
 */
@Service
public class RbacManagerServiceImpl implements RbacManagerService {
    @Autowired(required = false)
    RbacManagerDao rbacManagerDao;

    @Override
    public ResponseEntity findAll(Integer currentPage, Integer pageSize, RbacManager rbacManager) {
        PageHelper.startPage(currentPage, pageSize);
        System.out.println(rbacManager);
        List<RbacManager> rbacManagerList = rbacManagerDao.findAll(rbacManager);
        PageInfo<RbacManager> pageInfo = PageInfo.of(rbacManagerList);
        ResponseEntity<PageInfo<RbacManager>> responseEntity = new ResponseEntity<>();
        if (rbacManagerList.size() > 0){
            responseEntity.setCode(200);
            responseEntity.setMessage("查询成功");
            responseEntity.setData(pageInfo);
        }else{
            responseEntity.setCode(500);
            responseEntity.setMessage("查询失败");
            responseEntity.setData(null);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity delUser(int id) {
        ResponseEntity<List> responseEntity = new ResponseEntity<>();
        if (rbacManagerDao.deleteById(id) > 0){
            responseEntity.setCode(200);
            responseEntity.setMessage("删除成功");
            responseEntity.setData(null);
        }else{
            responseEntity.setCode(500);
            responseEntity.setMessage("删除失败");
            responseEntity.setData(null);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity addUser(RbacManager rbacManager) {
        ResponseEntity<Object> responseEntity = new ResponseEntity<>();
        if (rbacManagerDao.addUser(rbacManager) > 0) {
            responseEntity.setCode(200);
            responseEntity.setMessage("添加成功");
            responseEntity.setData(null);
        }else{
            responseEntity.setCode(500);
            responseEntity.setMessage("添加失败");
            responseEntity.setData(null);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity updateUser(RbacManager rbacManager) {
        ResponseEntity<Object> responseEntity = new ResponseEntity<>();
        if (rbacManagerDao.updateUser(rbacManager) > 0) {
            responseEntity.setCode(200);
            responseEntity.setMessage("修改成功");
            responseEntity.setData(null);
        }else{
            responseEntity.setCode(500);
            responseEntity.setMessage("修改失败");
            responseEntity.setData(null);
        }
        return responseEntity;
    }
}
