package com.woniuxy.dao;

import com.woniuxy.entity.RbacManager;

import java.util.List;

/**
 * @ClassName: RbacManagerDao
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/21 15:26
 */
public interface RbacManagerDao {

    List<RbacManager> findAll(RbacManager rbacManager);

    int deleteById(int id);

    int addUser(RbacManager rbacManager);

    int updateUser(RbacManager rbacManager);
}
