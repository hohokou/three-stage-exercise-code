package com.woniuxy.impl;

import com.woniuxy.Person;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @ClassName: Chinese
 * @Description: 中国人
 * @author: HOHOKOU
 * @date: 2022/1/17 11:35
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Chinese implements Person {
    private String name;
    private int age;
    private American son;
    private List<Integer> money;
    private Map<Integer, Integer> numbers;
    private Set<Integer> SetMoney;
    private List<Map<Integer,Integer>> listMap;
    private List<American> listAmerican;
    public void eat() {
        System.out.println("爱吃火锅");
    }
}
