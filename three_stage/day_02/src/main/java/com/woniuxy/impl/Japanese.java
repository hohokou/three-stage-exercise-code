package com.woniuxy.impl;

import com.woniuxy.Person;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @ClassName: Japanese
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/1/18 15:56
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Japanese implements Person {
    private String name;
    private int age;
    private American dad;
    private List<Integer> money;
    private Map<Integer, Integer> numbers;
    private Set<Integer> SetMoney;
    private List<Map<Integer,Integer>> listMap;
    private List<American> listAmerican;


    @Override
    public void eat() {
        System.out.println("报紧tom爹的大腿");
    }
}
