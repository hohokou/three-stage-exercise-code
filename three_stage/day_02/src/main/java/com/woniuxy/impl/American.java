package com.woniuxy.impl;

import com.woniuxy.Person;
import lombok.Data;

/**
 * @ClassName: American
 * @Description: 美国人
 * @author: HOHOKOU
 * @date: 2022/1/17 11:36
 */
@Data
public class American implements Person {
    private String name;

    public void eat() {
        System.out.println("自由美利坚，枪战每一天！");
    }
}

