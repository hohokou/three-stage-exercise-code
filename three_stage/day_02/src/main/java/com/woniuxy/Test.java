package com.woniuxy;

/**
 * @ClassName: Test
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/1/18 14:54
 */
class A {

    public A() {
        System.out.println("class A 1");
    }

    {
        System.out.println("I'm A class 2");
    }

    static {
        System.out.println("class A static 3");
    }
}

class B extends A {
    public B() {
        System.out.println("class B 4");
    }

    {
        System.out.println("I'm B class 5");
    }

    static {
        System.out.println("class B static 6");
    }

    public static void main(String[] args) {
        new B();
    }
}


public class Test {
}
