package com.woniuxy.autowire;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @ClassName: AutowireTest
 * @Description: 测试自动装配
 * @author: HOHOKOU
 * @date: 2022/1/18 17:13
 */
public class AutowireTest {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("/applicationContext01.xml");
        Man man = classPathXmlApplicationContext.getBean("man", Man.class);
        System.out.println(man);
        Man man1 = classPathXmlApplicationContext.getBean("man1", Man.class);
        System.out.println(man1);


    }
}
