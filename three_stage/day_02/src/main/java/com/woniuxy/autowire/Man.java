package com.woniuxy.autowire;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @ClassName: Man
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/1/18 17:14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Man {
    String name;
    Wife wife;
}
