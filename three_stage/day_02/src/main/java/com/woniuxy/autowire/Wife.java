package com.woniuxy.autowire;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName: Wife
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/1/18 17:14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Wife {
    String name;
}
