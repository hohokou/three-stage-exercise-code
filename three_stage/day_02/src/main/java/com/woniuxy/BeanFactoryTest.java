package com.woniuxy;


import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

/**
 * @ClassName: BeanFactoryTest
 * @Description: BeanFactory是在调用getBean之后再创建对象，而applicationContext 是在解析完xml之后就创建对象的，前提 xml中的scope属性没有设置多例
 * @author: HOHOKOU
 * @date: 2022/1/18 11:06
 */
public class BeanFactoryTest {
    public static void main(String[] args) {
//        BeanFactoryTest
//        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("/applicationContext.xml"));
//        Person american = beanFactory.getBean("american", Person.class);
//        american.eat();

//        文件路径解析xml
//        ApplicationContext applicationContext = new FileSystemXmlApplicationContext("D:\\code\\IdeaProjects\\three-stage-exercise-code\\three_stage\\day_02\\src\\main\\resources\\applicationContext.xml");
//        Person american1 = applicationContext.getBean("american3", Person.class);
//        System.out.println(american1);
//        american1.eat();

//        项目路径解析xml
//        ApplicationContext ClassPathXmlApplicationContext = new ClassPathXmlApplicationContext("/applicationContext.xml");
//        Person chinese = ClassPathXmlApplicationContext.getBean("chinese", Person.class);
//        chinese.eat();

        //setter注入测试
        ApplicationContext ClassPathXmlApplicationContext = new ClassPathXmlApplicationContext("/applicationContext.xml");
        Person japanese = ClassPathXmlApplicationContext.getBean("japanese", Person.class);
        System.out.println(japanese);

        //构造注入测试
        Person chinese = ClassPathXmlApplicationContext.getBean("chinese1", Person.class);
        System.out.println(chinese);
    }
}
