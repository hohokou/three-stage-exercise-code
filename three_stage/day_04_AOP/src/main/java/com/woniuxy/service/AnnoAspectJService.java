package com.woniuxy.service;

import org.springframework.stereotype.Service;

/**
 * @ClassName: AspectJService
 * @Description: 学习AspectJ
 * @author: HOHOKOU
 * @date: 2022/1/20 16:43
 */
@Service //这是一个service
public interface AnnoAspectJService {
    void methodOne();
    void methodTwo();
}
