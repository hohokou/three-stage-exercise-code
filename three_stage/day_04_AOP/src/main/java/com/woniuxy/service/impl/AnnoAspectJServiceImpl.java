package com.woniuxy.service.impl;

import com.woniuxy.service.AnnoAspectJService;
import org.springframework.stereotype.Service;

/**
 * @ClassName: AnnoAspectJServiceImpl
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/1/20 17:34
 */
@Service
public class AnnoAspectJServiceImpl implements AnnoAspectJService {
    @Override
    public void methodOne() {
        System.out.println("AspectJServiceImpl.methodOne 方法执行");
    }

    @Override
    public void methodTwo() {
        System.out.println("AspectJServiceImpl.methodTwo  方法执行");
    }
}
