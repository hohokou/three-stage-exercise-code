package com.woniuxy.service.impl;

import com.woniuxy.service.AspectJService;

/**
 * @ClassName: AspectJServiceImpl
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/1/20 16:44
 */
public class AspectJServiceImpl implements AspectJService {
    @Override
    public void methodOne() {
        System.out.println("AspectJServiceImpl.methodOne 方法执行");
    }

    @Override
    public void methodTwo() {
        System.out.println("AspectJServiceImpl.methodTwo  方法执行");
    }
}
