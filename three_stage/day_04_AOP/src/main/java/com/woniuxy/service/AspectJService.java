package com.woniuxy.service;

/**
 * @ClassName: AspectJService
 * @Description: 学习AspectJ
 * @author: HOHOKOU
 * @date: 2022/1/20 16:43
 */
public interface AspectJService {
    void methodOne();
    void methodTwo();
}
