package com.woniuxy.annoaspectj;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @ClassName: MyAspectJ
 * @Description: 面向切面
 * @author: HOHOKOU
 * @date: 2022/1/20 16:12
 */
@Aspect //aop需要用，表示这个类是注解
@Component //spring需要，表示这个类是组件，可以帮我们自动注册成bean id是类名首字母小写 myAspectJAnno
public class MyAspectJAnno {
    @Pointcut("execution(* com.woniuxy.service.*.*(..))")
    public void myPointCut(){}
    //前置通知
    @Before("myPointCut()")
    public void myBefore(JoinPoint joinPoint) {
        System.out.println("前置方法执行！");
        System.out.println("目标的注解有" + Arrays.toString(joinPoint.getTarget().getClass().getAnnotations()));
        System.out.println("被织入的目标方法是" + joinPoint.getSignature().getName());
    }

    //后置通知
    @AfterReturning("myPointCut()")
    public void myAfterReturning(JoinPoint joinPoint) {
        System.out.println("后置方法执行！模拟事务提交");
    }

    //环绕通知 不推荐使用
    @Around("myPointCut()")
    public Object myAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("环绕方法开始执行");
        Object proceed = proceedingJoinPoint.proceed();
        System.out.println("环绕方法停止执行");
        return proceed;
    }

    //异常通知
    @AfterThrowing(value = "myPointCut()",throwing = "e")
    public void myAfterThrowing(JoinPoint joinPoint, Throwable e) {
        System.out.println("异常通知执行");
        System.out.println("异常消息：" + e.getMessage());
    }

    //最终通知
    @After("myPointCut()")
    public void  myAfter(){
        System.out.println("最终通知执行，可以用来关闭资源");
    }
}
