package com.woniuxy.util;

import com.woniuxy.aspect.MyAspect;

import java.lang.reflect.Proxy;

/**
 * @ClassName: myProxy
 * @Description: jdk-动态代理
 * @author: HOHOKOU
 * @date: 2022/1/20 11:28
 */
public class MyProxy {
    public static Object getProxy(Object target) {
        return Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), (proxy, method, args) -> {
            MyAspect.frontMethod();
            Object invoke = method.invoke(target, args);
            MyAspect.rearMethod();
            return invoke;
        });
    }

    public static <T> T getProxy(Object target, Class<T> requiredType) {
        return (T) getProxy(target);
    }
}
