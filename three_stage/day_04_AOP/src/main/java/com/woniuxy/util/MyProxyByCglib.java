package com.woniuxy.util;

import com.woniuxy.aspect.MyAspect;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @ClassName: MyProxyByCglib
 * @Description: Cglib动态代理
 * @author: HOHOKOU
 * @date: 2022/1/20 14:21
 */
public class MyProxyByCglib {
    public static <T> T getProxy(Object target,Class<T> c){
       return (T) Enhancer.create(target.getClass(), new MethodInterceptor() {
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                MyAspect.frontMethod();
                Object invoke = method.invoke(target,objects);
                MyAspect.rearMethod();
                return invoke;
            }
        });
    }
}
