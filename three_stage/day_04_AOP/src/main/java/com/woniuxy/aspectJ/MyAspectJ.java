package com.woniuxy.aspectJ;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;

/**
 * @ClassName: MyAspectJ
 * @Description: 面向切面
 * @author: HOHOKOU
 * @date: 2022/1/20 16:12
 */
public class MyAspectJ {
    //前置通知
    public void myBefore(JoinPoint joinPoint) {
        System.out.println("前置方法执行！");
        System.out.println("目标类是" + joinPoint.getTarget().getClass().getName());
        System.out.println("被织入的目标方法是" + joinPoint.getSignature().getName());
    }

    //后置通知
    public void myAfterReturning(JoinPoint joinPoint) {
        System.out.println("后置方法执行！模拟事务提交");
    }

    //环绕通知
    public Object myAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("环绕方法开始执行");
        Object proceed = proceedingJoinPoint.proceed();
        System.out.println("环绕方法停止执行");
        return proceed;
    }

    //异常通知
    public void myAfterThrowing(JoinPoint joinPoint, Throwable e) {
        System.out.println("异常通知执行");
        System.out.println("异常消息：" + e.getMessage());
    }

    //最终通知
    public void  myAfter(){
        System.out.println("最终通知执行，可以用来关闭资源");
    }
}
