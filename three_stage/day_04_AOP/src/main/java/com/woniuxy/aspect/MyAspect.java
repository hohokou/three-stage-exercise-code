package com.woniuxy.aspect;

/**
 * @ClassName: myAspect
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/1/20 12:07
 */
public class MyAspect {
    public static void frontMethod(){
        System.out.println("切面前置方法执行");
    }

    public static void rearMethod(){
        System.out.println("切面后置方法执行");
    }
}
