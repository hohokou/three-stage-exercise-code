package com.woniuxy.aspectJ;

import com.woniuxy.service.AspectJService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.*;

public class MyAspectJTest {
    //测试AspectJ AOP切面
    @Test
    public void aspectJAopTest() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("aspectJAOP.xml");
        AspectJService aspectJService = applicationContext.getBean("aspectJService", AspectJService.class);
        aspectJService.methodOne();
        System.out.println("==================================");
        aspectJService.methodTwo();

    }
}