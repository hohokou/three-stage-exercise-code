package com.woniuxy.service.impl;

import com.woniuxy.service.EmpService;
import com.woniuxy.util.MyProxy;
import com.woniuxy.util.MyProxyByCglib;
import org.junit.Test;

public class EmpServiceImplTest {

    @Test
    public void thisIsMethod() {
/*        EmpService proxy = MyProxy.getProxy(new EmpServiceImpl(), EmpService.class);
        proxy.ThisIsMethod();*/

        EmpService proxy1 = MyProxyByCglib.getProxy(new EmpServiceImpl(), EmpService.class);
        proxy1.ThisIsMethod();
    }
}