package com.woniuxy.annoaspectj;

import com.woniuxy.service.AnnoAspectJService;
import com.woniuxy.service.AspectJService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.*;

public class MyAspectJAnnoTest {
    //注解测试AspectJ AOP切面
    @Test
    public void aspectJAopTest() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("annoaspectJAOP.xml");
        AnnoAspectJService annoAspectJService = applicationContext.getBean("annoAspectJServiceImpl", AnnoAspectJService.class);
        annoAspectJService.methodOne();
        System.out.println("==================================");
        annoAspectJService.methodTwo();

    }
}