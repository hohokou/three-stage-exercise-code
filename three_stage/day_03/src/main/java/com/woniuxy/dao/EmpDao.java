package com.woniuxy.dao;

import com.woniuxy.entity.Stu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmpDao {
    //查找所有数据
    List<Stu> findAll();

    //添加数据
    int insertStu(Stu stu);

    //修改数据
    int updateStuNameBySid(@Param("sid") int sid, @Param("name")String name);

    //删除数据
    int delBySid(@Param("sid") int sid);
}
