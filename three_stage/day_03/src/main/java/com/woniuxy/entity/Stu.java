package com.woniuxy.entity;


public class Stu {
    private long sid;
    private String sname;
    private long age;
    private String gender;
    private String province;
    private long tuition;

    public Stu() {
    }

    public Stu(long sid, String sname, long age, String gender, String province, long tuition) {
        this.sid = sid;
        this.sname = sname;
        this.age = age;
        this.gender = gender;
        this.province = province;
        this.tuition = tuition;
    }

    public long getSid() {
        return sid;
    }

    public void setSid(long sid) {
        this.sid = sid;
    }


    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }


    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }


    public long getTuition() {
        return tuition;
    }

    public void setTuition(long tuition) {
        this.tuition = tuition;
    }

    @Override
    public String toString() {
        return "Stu{" +
                "sid=" + sid +
                ", sname='" + sname + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", province='" + province + '\'' +
                ", tuition=" + tuition +
                '}';
    }
}
