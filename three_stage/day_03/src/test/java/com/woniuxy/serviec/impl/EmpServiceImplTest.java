package com.woniuxy.serviec.impl;

import com.woniuxy.entity.Stu;
import com.woniuxy.serviec.EmpService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.*;

public class EmpServiceImplTest {


    @Test
    public void findAll() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        EmpService empService = applicationContext.getBean("empService", EmpService.class);
        System.out.println(empService.findAll());
    }

    @Test
    public void insertStu() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        EmpService empService = applicationContext.getBean("empService", EmpService.class);
        Stu stu = new Stu(0,"张三",29,"男","四川",8000);
        if (empService.insertStu(stu)) {
            System.out.println("成功");
        }
    }

    @Test
    public void updateStuNameBySid() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        EmpService empService = applicationContext.getBean("empService", EmpService.class);
        if (empService.updateStuNameBySid(56,"李四")) {
            System.out.println("成功");
        }
    }

    @Test
    public void delBySid() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        EmpService empService = applicationContext.getBean("empService", EmpService.class);
        if (empService.delBySid(56)) {
            System.out.println("成功");
        }
    }
}