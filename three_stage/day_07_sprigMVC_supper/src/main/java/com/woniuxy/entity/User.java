package com.woniuxy.entity;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: User
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/1/25 15:56
 */
@Data
public class User {
    private String name;
    private Integer age;
    private String gender;
    private List<String> stuNames;
    private Map<String, Integer> stuAges;
}
