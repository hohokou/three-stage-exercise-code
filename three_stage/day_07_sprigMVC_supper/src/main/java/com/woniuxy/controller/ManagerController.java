package com.woniuxy.controller;

import com.woniuxy.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: ManagerController
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/1/25 14:45
 */
@Controller
@RequestMapping("/managerController")
public class ManagerController{
    @RequestMapping("/list")
    @ResponseBody
    public List<String> doGet(){
        List<String> list = new ArrayList<>();
        list.add("123");
        list.add("123");
        list.add("123");
        return list;
    }

    @RequestMapping("/user")
    @ResponseBody
    public User doGetUser(){
        User user = new User();
        user.setName("张三");
        user.setAge(18);
        user.setGender("男");
        return user;
    }
}
