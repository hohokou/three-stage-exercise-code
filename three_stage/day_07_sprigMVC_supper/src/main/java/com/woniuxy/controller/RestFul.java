package com.woniuxy.controller;

import com.woniuxy.entity.User;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName: RestFul
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/1/25 17:10
 */

@RestController
public class RestFul {
//    @RequestMapping(value = "/user",method = RequestMethod.POST)
    @PostMapping("/user")
    public String addUser(@RequestBody User user){
        System.out.println("RestFul.addUser方法执行");
        return user.toString();
    }

    //注意参数注解的添加
    @DeleteMapping("/user/{id}/{name}")
    public String deleteUser(@PathVariable int id,@PathVariable String name){
        System.out.println("RestFul.deleteUser执行");
        return id + "====" + name;
    }

    @GetMapping("/user/{userName}")
    public User getUser(@PathVariable("userName") String name){
        System.out.println("RestFul.getUser执行");
        User user = new User();
        user.setName(name);
        return user;
    }
}
