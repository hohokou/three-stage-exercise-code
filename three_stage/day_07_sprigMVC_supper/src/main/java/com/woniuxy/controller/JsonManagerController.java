package com.woniuxy.controller;

import com.woniuxy.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: JsonManagerController
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/1/25 16:11
 */
@RestController
@RequestMapping("/json")
public class JsonManagerController {
    @RequestMapping("/addUser")
    public String addOne(@RequestBody User user,String id){
        System.out.println("JsonManagerController.addOne 方法执行成功！" + id);
        return user.toString();
    }
}
