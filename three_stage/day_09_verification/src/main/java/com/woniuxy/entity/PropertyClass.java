package com.woniuxy.entity;

import javax.validation.constraints.NotEmpty;

/**
 * @ClassName: PropertyClass
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/10 12:33
 */
public class PropertyClass {
    @NotEmpty(message = "属性不能为空！")
    private String property;

    public PropertyClass() {
    }

    public PropertyClass(String property) {
        this.property = property;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    @Override
    public String toString() {
        return "PropertyClass{" +
                "property='" + property + '\'' +
                '}';
    }
}
