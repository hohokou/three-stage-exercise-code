package com.woniuxy.entity;

import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @ClassName: User
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/10 11:27
 */
public class User {
    @NotEmpty(message = "账号不能为空")
    @Length(min = 1,max = 16,message = "账号长度只能为1~16位")
    private String account;
    @NotEmpty(message = "密码不能为空")
    @Length(min = 1,max = 16,message = "密码长度只能为1~16位")
    private String password;
    @Email
    private String email;
    @Pattern(regexp = "^134[0-8]\\d{7}$|^13[^4]\\d{8}$|^14[5-9]\\d{8}$|^15[^4]\\d{8}$|^16[6]\\d{8}$|^17[0-8]\\d{8}$|^18[\\d]{9}$|^19[8,9]\\d{8}$",message = "手机号码格式不正确")
    private String phone;
    @Valid
    @NotNull
    private PropertyClass propertyClass;


    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public PropertyClass getPropertyClass() {
        return propertyClass;
    }

    public void setPropertyClass(PropertyClass propertyClass) {
        this.propertyClass = propertyClass;
    }

    @Override
    public String toString() {
        return "User{" +
                "account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", propertyClass=" + propertyClass +
                '}';
    }
}
