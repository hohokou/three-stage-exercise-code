package com.woniuxy.controller;

import com.woniuxy.entity.ResponseEntity;
import com.woniuxy.entity.User;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

/**
 * @ClassName: UserController
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/10 11:27
 */
@RestController
@RequestMapping("/userController")
public class UserController {
    @RequestMapping("/testUser")
    public ModelAndView testUser(@Valid @RequestBody User user, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        if (bindingResult.hasErrors()) {
            FieldError fieldError = bindingResult.getFieldError();
            modelAndView.addObject("binding", fieldError.getField() + "" + fieldError.getDefaultMessage());
            modelAndView.setViewName("/WEB-INF/jsp/error.jsp");
        }

        return modelAndView;
    }
}
