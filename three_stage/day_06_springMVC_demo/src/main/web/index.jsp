<%--
  Created by IntelliJ IDEA.
  User: HOHOKOU
  Date: 2022/1/24
  Time: 11:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>$Title$</title>
  </head>
  <body>
  <hr/>
  <h3>字符串传参</h3>
  <form action="/day_06_springMVC_demo/form/StringValue" method="post">
    姓名：<input type="text" name="name">
    <button>提交</button>
  </form>
  <hr/>
  <hr/>
  <h3>数组传参</h3>
  <form action="/day_06_springMVC_demo/form/ArrayValue" method="post">
    <input type="checkbox" name="arr" value="one">1
    <input type="checkbox" name="arr" value="two">2
    <input type="checkbox" name="arr" value="three">3
    <button>提交</button>
  </form>
  <hr/>

  <hr/>
  <h3>List传参</h3>
  <form action="/day_06_springMVC_demo/form/ListValue" method="post">
    <input type="checkbox" name="arr" value="one">1
    <input type="checkbox" name="arr" value="two">2
    <input type="checkbox" name="arr" value="three">3
    <button>提交</button>
  </form>
  <hr/>

  <hr/>
  <h3>Map传参</h3>
  <form action="/day_06_springMVC_demo/form/MapValue" method="post">
    用户名：<input type="text" name="userName">
    密码：<input type="password" name="password">
    邮箱：<input type="email" name="email">
    <button>提交</button>
  </form>
  <hr/>

  <hr/>
  <h3>对象传参</h3>
  <form action="/day_06_springMVC_demo/form/UserValue" method="post">
    用户名：<input type="text" name="name">
    年龄：<input type="number" name="age">
    邮箱：<input type="email" name="email">
    性别：<input type="text" name="gender">
    <button>提交</button>
  </form>
  <hr/>

  <hr/>
  <h3>对象内对象集合传参</h3>
  <form action="/day_06_springMVC_demo/form/ClassValue" method="post">
      班级编号：<input type="number" name="classNumber">
      班级名称：<input type="text" name="className"><br/>
      用户名：<input type="text" name="users[0].name">
      年龄：<input type="number" name="users[0].age">
      邮箱：<input type="email" name="users[0].email">
      性别：<input type="text" name="users[0].gender"><br/>
    用户名：<input type="text" name="users[1].name">
    年龄：<input type="number" name="users[1].age">
    邮箱：<input type="email" name="users[1].email">
    性别：<input type="text" name="users[1].gender"><br/>
      <button>提交</button>
  </form>

  </body>
</html>
