package com.woniuxy.controller;

import com.woniuxy.entity.Clazz;
import com.woniuxy.entity.User;
import org.aspectj.weaver.ast.Var;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: FormSendParamsController
 * @Description: 用于接收前端from表单传来的参数
 * @author: HOHOKOU
 * @date: 2022/1/24 16:00
 */
@Controller
@RequestMapping("/form")
public class FormSendParamsController {
    @RequestMapping("/StringValue")
    public ModelAndView receiveParameters(String name) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("msg", "接收的参数是" + name);
        modelAndView.setViewName("result");
        return modelAndView;
    }

    @RequestMapping("/ArrayValue")
    public ModelAndView receiveParameters(String[] arr) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("msg", "接收的参数是" + Arrays.toString(arr));
        modelAndView.setViewName("result");
        return modelAndView;
    }

    @RequestMapping("/ListValue")
    public ModelAndView receiveParameters(@RequestParam List arr) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("msg", "接收的参数是" + arr);
        modelAndView.setViewName("result");
        return modelAndView;
    }

    @RequestMapping("/MapValue")
    public ModelAndView receiveParameters(@RequestParam Map map) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("msg", "接收的参数是" + map);
        modelAndView.setViewName("result");
        return modelAndView;
    }


    @RequestMapping("/UserValue")
    public ModelAndView receiveParameters(User user) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("msg", "接收的参数是" + user.toString());
        modelAndView.setViewName("result");
        return modelAndView;
    }

    @RequestMapping("/ClassValue")
    public ModelAndView receiveParameters(Clazz clazz) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("msg", "接收的参数是" + clazz.toString());
        modelAndView.setViewName("result");
        return modelAndView;
    }

}
