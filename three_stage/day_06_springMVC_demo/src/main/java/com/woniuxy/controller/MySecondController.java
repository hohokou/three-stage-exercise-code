package com.woniuxy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @ClassName: MySecondController
 * @Description: 推荐写法
 * @author: HOHOKOU
 * @date: 2022/1/24 14:54
 */
@Controller
@RequestMapping("/user")
public class MySecondController {
    @RequestMapping("/methodOne")
    public ModelAndView methodOne(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("msg","01.com");
        modelAndView.setViewName("/index.jsp");
        return modelAndView;
    }

    @RequestMapping("/methodTwo")
    public ModelAndView methodTwo(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("msg","R6.com");
        modelAndView.setViewName("/index.jsp");
        return modelAndView;
    }
}
