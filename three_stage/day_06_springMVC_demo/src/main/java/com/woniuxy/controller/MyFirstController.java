package com.woniuxy.controller;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @ClassName: MyFirstController
 * @Description: 我的第一个Controller
 * @author: HOHOKOU
 * @date: 2022/1/24 12:06
 */
public class MyFirstController implements Controller {
    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("msg","HelloWorld!");
        modelAndView.setViewName("/index.jsp");
        return modelAndView;
    }
}
