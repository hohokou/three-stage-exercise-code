package com.woniuxy.utills;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @ClassName: MyHandlerInterceptor
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/11 11:01
 */
public class MyHandlerInterceptor01 implements HandlerInterceptor {
    long start;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        start = System.currentTimeMillis();
        System.out.println("MyHandlerInterceptor.preHandle1" + "执行");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        long end = System.currentTimeMillis();
        System.out.println((end - start) + "ms");
        System.out.println("MyHandlerInterceptor.postHandle1" + "执行");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("MyHandlerInterceptor.afterCompletion1" + "执行");
    }
}
