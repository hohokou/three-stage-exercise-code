package com.woniuxy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @ClassName: TestController
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/2/11 11:03
 */
@Controller
@RequestMapping("/test")
public class TestController {
    @RequestMapping("/methodOne")
    public ModelAndView testMethod(){
        System.out.println("控制器方法执行");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("msg","121");
        modelAndView.setViewName("/WEB-INF/jsp/Test01.jsp");
        return modelAndView;
    }
}
