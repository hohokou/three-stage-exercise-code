package com.woniuxy.service;

import com.woniuxy.entity.Stu;

import java.util.List;

public interface EmpService {
    List<Stu> findAll();

    void insertStu(Stu stu);

    boolean updateStuNameBySid (int sid,String name);

    boolean delBySid(int sid);
}
