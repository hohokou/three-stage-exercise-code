package com.woniuxy.service.impl;

import com.woniuxy.dao.EmpDao;
import com.woniuxy.entity.Stu;
import com.woniuxy.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName: EmpServiceImpl
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/1/19 15:57
 */
@Service("empService")
public class EmpServiceImpl implements EmpService {

    @Autowired
    private EmpDao empDao;

    @Override
    public List<Stu> findAll() {
        return empDao.findAll();
    }

    @Override
    public void insertStu(Stu stu) {
         empDao.insertStu(stu);
         int i = 1/0;
        System.out.println(empDao.findAll());
    }

    @Override
    public boolean updateStuNameBySid(int sid, String name) {
        return empDao.updateStuNameBySid(sid,name) > 0;
    }

    @Override
    public boolean delBySid(int sid) {
        return empDao.delBySid(sid)>0;
    }
}
