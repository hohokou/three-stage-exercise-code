package com.woniuxy.impl;

import com.woniuxy.Person;

/**
 * @ClassName: American
 * @Description: 美国人
 * @author: HOHOKOU
 * @date: 2022/1/17 11:36
 */
public class American implements Person {
    public void eat() {
        System.out.println("");
    }
}
