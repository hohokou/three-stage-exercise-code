package com.woniuxy.impl;

import com.woniuxy.Person;

/**
 * @ClassName: Chinese
 * @Description: 中国人
 * @author: HOHOKOU
 * @date: 2022/1/17 11:35
 */
public class Chinese implements Person {
    public void eat() {
        System.out.println("爱吃火锅");
    }
}
