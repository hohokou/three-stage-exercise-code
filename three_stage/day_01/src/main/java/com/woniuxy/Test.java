package com.woniuxy;

import com.woniuxy.factories.BeanFactory;
import com.woniuxy.factories.BeanFactory01;

/**
 * @ClassName: Test
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/1/17 12:39
 */
public class Test {
    public static void main(String[] args) {
        Person chinese1 = new BeanFactory01().getBean("chinese");
        Person chinese2 = new BeanFactory01().getBean("chinese");
        Person chinese3 = new BeanFactory01().getBean("chinese");
        System.out.println(chinese1);
        System.out.println(chinese2);
        System.out.println(chinese3);

        Person chinese4 = new BeanFactory().getBean("chinese");
        Person chinese5 = new BeanFactory().getBean("chinese");
        Person chinese6 = new BeanFactory().getBean("chinese");
        System.out.println(chinese4);
        System.out.println(chinese5);
        System.out.println(chinese6);

    }
}
