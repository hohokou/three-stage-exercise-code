package com.woniuxy.factories;

import com.woniuxy.Person;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @ClassName: BeanFactory
 * @Description: 单例模式工厂类
 * @author: HOHOKOU
 * @date: 2022/1/17 12:15
 */
public class BeanFactory {
    private static Map<String, Person> map;

    static {
        map = new HashMap<>();
        Properties properties = new Properties();
        try {
            properties.load(BeanFactory.class.getResourceAsStream("/person.properties"));
            for (String key : properties.stringPropertyNames()) {
                String property = properties.getProperty(key);
                Person person = (Person) Class.forName(property).newInstance();
                map.put(key,person);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public Person getBean(String name) {
        return map.get(name);
    }
}
