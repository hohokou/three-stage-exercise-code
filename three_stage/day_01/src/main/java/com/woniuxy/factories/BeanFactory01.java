package com.woniuxy.factories;

import com.woniuxy.Person;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @ClassName: BeanFactory01
 * @Description: 多例模式工厂类
 * @author: HOHOKOU
 * @date: 2022/1/17 14:20
 */
public class BeanFactory01 {
    private static Map<String, String> map;

    static {
        map = new HashMap<>();
        Properties properties = new Properties();
        try {
            properties.load(BeanFactory01.class.getResourceAsStream("/person.properties"));
            for (String key : properties.stringPropertyNames()) {
                map.put(key,properties.getProperty(key));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Person getBean(String name) {
        Person person = null;
        try {
            person = (Person) Class.forName(map.get(name)).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return person;
    }
}
