package com.woniuxy;


import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

/**
 * @ClassName: SpringTest
 * @Description:
 * @author: HOHOKOU
 * @date: 2022/1/17 15:42
 */
public class SpringTest {
    public static void main(String[] args) {
        BeanFactory BeanFactory = new XmlBeanFactory(new ClassPathResource("/applicationContext.xml"));
        Person chinese = (Person) BeanFactory.getBean("chinese");
        chinese.eat();

    }
}
